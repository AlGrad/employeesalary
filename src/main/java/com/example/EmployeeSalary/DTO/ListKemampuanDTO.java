package com.example.EmployeeSalary.DTO;

import com.example.EmployeeSalary.models.Karyawan;
import com.example.EmployeeSalary.models.Kemampuan;

public class ListKemampuanDTO {
	private int idListKemampuan;
	private KaryawanDTO karyawan;
	private KemampuanDTO kemampuan;
	private Integer nilaiKemampuan;
	
	public ListKemampuanDTO() {
		super();
	}
	public ListKemampuanDTO(int idListKemampuan, KaryawanDTO karyawan, KemampuanDTO kemampuan, Integer nilaiKemampuan) {
		super();
		this.idListKemampuan = idListKemampuan;
		this.karyawan = karyawan;
		this.kemampuan = kemampuan;
		this.nilaiKemampuan = nilaiKemampuan;
	}
	public int getIdListKemampuan() {
		return idListKemampuan;
	}
	public void setIdListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}
	public KaryawanDTO getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(KaryawanDTO karyawan) {
		this.karyawan = karyawan;
	}
	public KemampuanDTO getKemampuan() {
		return kemampuan;
	}
	public void setKemampuan(KemampuanDTO kemampuan) {
		this.kemampuan = kemampuan;
	}
	public Integer getNilaiKemampuan() {
		return nilaiKemampuan;
	}
	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
	
	

}
