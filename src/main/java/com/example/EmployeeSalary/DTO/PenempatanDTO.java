package com.example.EmployeeSalary.DTO;

import java.math.BigDecimal;

public class PenempatanDTO {
	private int idPenempatan;
	private String kotaPenempatan;
	private BigDecimal umkPenempatan;
	
	
	public PenempatanDTO() {
		super();
	}
	public PenempatanDTO(int idPenempatan, String kotaPenempatan, BigDecimal umkPenempatan) {
		super();
		this.idPenempatan = idPenempatan;
		this.kotaPenempatan = kotaPenempatan;
		this.umkPenempatan = umkPenempatan;
	}
	public int getIdPenempatan() {
		return idPenempatan;
	}
	public void setIdPenempatan(int idPenempatan) {
		this.idPenempatan = idPenempatan;
	}
	public String getKotaPenempatan() {
		return kotaPenempatan;
	}
	public void setKotaPenempatan(String kotaPenempatan) {
		this.kotaPenempatan = kotaPenempatan;
	}
	public BigDecimal getUmkPenempatan() {
		return umkPenempatan;
	}
	public void setUmkPenempatan(BigDecimal umkPenempatan) {
		this.umkPenempatan = umkPenempatan;
	}
	
	

}
