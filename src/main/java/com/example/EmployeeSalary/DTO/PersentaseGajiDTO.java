package com.example.EmployeeSalary.DTO;

import java.math.BigDecimal;

import com.example.EmployeeSalary.models.Posisi;

public class PersentaseGajiDTO {
	private int idPresentaseGaji;
	private PosisiDTO posisi;
	private Integer idTingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;
	
	public PersentaseGajiDTO() {
		super();
	}
	public PersentaseGajiDTO(int idPresentaseGaji, PosisiDTO posisi, Integer idTingkatan, BigDecimal besaranGaji,
			Integer masaKerja) {
		super();
		this.idPresentaseGaji = idPresentaseGaji;
		this.posisi = posisi;
		this.idTingkatan = idTingkatan;
		this.besaranGaji = besaranGaji;
		this.masaKerja = masaKerja;
	}
	public int getIdPresentaseGaji() {
		return idPresentaseGaji;
	}
	public void setIdPresentaseGaji(int idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}
	public PosisiDTO getPosisi() {
		return posisi;
	}
	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}
	public Integer getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(Integer idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public BigDecimal getBesaranGaji() {
		return besaranGaji;
	}
	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
	
	

}
