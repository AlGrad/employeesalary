package com.example.EmployeeSalary.DTO;

public class UserIdDTO {
	private int idUser;
	private String username;
	
	public UserIdDTO() {
		super();
	}
	public UserIdDTO(int idUser, String username) {
		super();
		this.idUser = idUser;
		this.username = username;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	

}
