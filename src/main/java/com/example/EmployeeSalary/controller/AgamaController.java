package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.AgamaDTO;
import com.example.EmployeeSalary.models.Agama;
import com.example.EmployeeSalary.repositories.AgamaRepository;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class AgamaController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	AgamaRepository agamaRepository;
	
	@GetMapping("/religion/readAll")
	public HashMap<String,Object>getAllReligion(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Agama>listAgamaEntity = (ArrayList<Agama>) agamaRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<AgamaDTO>listAgamaDTO = new ArrayList<AgamaDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Agama agama : listAgamaEntity) {
			//inisialisasi obj Publisher DTO 
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);

			
			listAgamaDTO.add(agamaDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all religion --> success");
		result.put("Data", listAgamaDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/religion/creates")
	public HashMap<String, Object> createReligion(@Valid @RequestBody AgamaDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Agama agamaEntity = modelMapper.map(body, Agama.class);
		//proses saving data ke database
	
		
		agamaRepository.save(agamaEntity);
		
		body.setIdAgama(agamaEntity.getIdAgama());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new religion --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/religion/update/{id}")
	public HashMap<String, Object> updateReligion(@PathVariable(value = "id") Integer idAgama, @Valid @RequestBody AgamaDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Agama agamaEntity = agamaRepository.findById(idAgama).orElseThrow(()-> new ResourceNotFoundException("Agama", "id", idAgama));
		
		
		agamaEntity = modelMapper.map(body, Agama.class);
		
		agamaEntity.setIdAgama(idAgama);
	
		
		agamaRepository.save(agamaEntity);
		
		body = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update religion --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/religion/delete/{id}")
	public HashMap<String, Object>deleteReligion(@PathVariable(value = "id")Integer idAgama,  AgamaDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Agama agamaEntity = agamaRepository.findById(idAgama)
				.orElseThrow(()-> new ResourceNotFoundException("Agama", "id", idAgama));
		
		agamaEntity = modelMapper.map(body, Agama.class);
		agamaEntity.setIdAgama(idAgama);
		
		agamaRepository.delete(agamaEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete religion --> success");
		result.put("Data", "[]");
		
		return result;
		
	}

}
