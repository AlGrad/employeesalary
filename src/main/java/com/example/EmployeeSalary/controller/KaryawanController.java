package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.KaryawanDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Karyawan;
import com.example.EmployeeSalary.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api")
public class KaryawanController {

	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	KaryawanRepository karyawanRepository;
	
	@GetMapping("/employees/readAll")
	public HashMap<String,Object>getAllEmployees(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Karyawan>listKaryawanEntity = (ArrayList<Karyawan>) karyawanRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<KaryawanDTO>listKaryawanDTO = new ArrayList<KaryawanDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Karyawan karyawan : listKaryawanEntity) {
			//inisialisasi obj Publisher DTO 
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);

			
			listKaryawanDTO.add(karyawanDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all employee --> success");
		result.put("Data", listKaryawanDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/employee/creates")
	public HashMap<String, Object> createEmployee(@Valid @RequestBody KaryawanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Karyawan karyawanEntity = modelMapper.map(body, Karyawan.class);
		//proses saving data ke database
	
		
		karyawanRepository.save(karyawanEntity);
		
		body.setIdKaryawan(karyawanEntity.getIdKaryawan());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new employee --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/employee/update/{id}")
	public HashMap<String, Object> updateEmployee(@PathVariable(value = "id") Integer idKaryawan, @Valid @RequestBody KaryawanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan).orElseThrow(()-> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		
		
		karyawanEntity = modelMapper.map(body, Karyawan.class);
		
		karyawanEntity.setIdKaryawan(idKaryawan);
	
		
		karyawanRepository.save(karyawanEntity);
		
		body = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update employee --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/employee/delete/{id}")
	public HashMap<String, Object>deleteEmployee(@PathVariable(value = "id")Integer idKaryawan,  KaryawanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan)
				.orElseThrow(()-> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		
		karyawanEntity = modelMapper.map(body, Karyawan.class);
		karyawanEntity.setIdKaryawan(idKaryawan);
		
		karyawanRepository.delete(karyawanEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete employee --> success");
		result.put("Data", "[]");
		
		return result;
		
	}


}
