package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.PenempatanDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Penempatan;
import com.example.EmployeeSalary.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api")
public class PenempatanController {

	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PenempatanRepository penempatanRepository;
	
	@GetMapping("/placement/readAll")
	public HashMap<String,Object>getAllPlacement(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Penempatan>listPenempatanEntity = (ArrayList<Penempatan>) penempatanRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<PenempatanDTO>listPenempatanDTO = new ArrayList<PenempatanDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Penempatan penempatan: listPenempatanEntity) {
			//inisialisasi obj Publisher DTO 
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);

			
			listPenempatanDTO.add(penempatanDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all placement --> success");
		result.put("Data", listPenempatanDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/placement/creates")
	public HashMap<String, Object> createPlacement(@Valid @RequestBody PenempatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Penempatan penempatanEntity = modelMapper.map(body, Penempatan.class);
		//proses saving data ke database
	
		
		penempatanRepository.save(penempatanEntity);
		
		body.setIdPenempatan(penempatanEntity.getIdPenempatan());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new placement --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/placement/update/{id}")
	public HashMap<String, Object> updatePlacement(@PathVariable(value = "id") Integer idPenempatan, @Valid @RequestBody PenempatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan).orElseThrow(()-> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		
		
		penempatanEntity = modelMapper.map(body, Penempatan.class);
		
		penempatanEntity.setIdPenempatan(idPenempatan);
	
		
		penempatanRepository.save(penempatanEntity);
		
		body = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update placement --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/placement/delete/{id}")
	public HashMap<String, Object>deleteReligion(@PathVariable(value = "id")Integer idPenempatan,  PenempatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan)
				.orElseThrow(()-> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		
		penempatanEntity = modelMapper.map(body, Penempatan.class);
		penempatanEntity.setIdPenempatan(idPenempatan);
		
		penempatanRepository.delete(penempatanEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete placement --> success");
		result.put("Data", "[]");
		
		return result;
		
	}


}
