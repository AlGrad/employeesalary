package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.PosisiDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.repositories.PosisiRepository;

@RestController
@RequestMapping("/api")
public class PosisiController {

	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PosisiRepository posisiRepository;
	
	@GetMapping("/position/readAll")
	public HashMap<String,Object>getAllPosition(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		//Mengambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Posisi>listPosisiEntity = (ArrayList<Posisi>) posisiRepository.findAll();
		
		
		//Membuat sebuah array list DTO
		ArrayList<PosisiDTO>listPosisiDTO = new ArrayList<PosisiDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Posisi posisi : listPosisiEntity) {
			//inisialisasi obj Publisher DTO 
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);

			
			listPosisiDTO.add(posisiDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all position --> success");
		result.put("Data", listPosisiDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/position/creates")
	public HashMap<String, Object> createPosition(@Valid @RequestBody PosisiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Posisi posisiEntity = modelMapper.map(body, Posisi.class);
		//proses saving data ke database
	
		
		posisiRepository.save(posisiEntity);
		
		body.setIdPosisi(posisiEntity.getIdPosisi());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new position --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/position/update/{id}")
	public HashMap<String, Object> updatePosition(@PathVariable(value = "id") Integer idPosisi, @Valid @RequestBody PosisiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Posisi posisiEntity = posisiRepository.findById(idPosisi).orElseThrow(()-> new ResourceNotFoundException("Posisi", "id", idPosisi));
		
		
		posisiEntity = modelMapper.map(body, Posisi.class);
		
		posisiEntity.setIdPosisi(idPosisi);
	
		
		posisiRepository.save(posisiEntity);
		
		body = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update position --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/position/delete/{id}")
	public HashMap<String, Object>deletePosition(@PathVariable(value = "id")Integer idPosisi,  PosisiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Posisi posisiEntity = posisiRepository.findById(idPosisi)
				.orElseThrow(()-> new ResourceNotFoundException("Posisi", "id", idPosisi));
		
		posisiEntity = modelMapper.map(body, Posisi.class);
		posisiEntity.setIdPosisi(idPosisi);
		
		posisiRepository.delete(posisiEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete position --> success");
		result.put("Data", "[]");
		
		return result;
		
	}


}
