package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.PersentaseGajiDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.PresentaseGaji;
import com.example.EmployeeSalary.repositories.PresentasiGajiRepository;

@RestController
@RequestMapping("/api")
public class PresentasiGajiController {

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PresentasiGajiRepository salaryRepository;
	
	@GetMapping("/salary/readAll")
	public HashMap<String,Object>getAllLevel(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		ArrayList<PresentaseGaji>listSalaryEntity = (ArrayList<PresentaseGaji>) salaryRepository.findAll();
				
		
		//Membuat sebuah array list DTO
		ArrayList<PersentaseGajiDTO>listSalaryDTO = new ArrayList<PersentaseGajiDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(PresentaseGaji salary : listSalaryEntity) {
			//inisialisasi obj Publisher DTO 
			PersentaseGajiDTO salaryDTO = modelMapper.map(salary, PersentaseGajiDTO.class);

			
			listSalaryDTO.add(salaryDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all salary employee  --> success");
		result.put("Data", listSalaryDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/salary/creates")
	public HashMap<String, Object> createSalary(@Valid @RequestBody PersentaseGajiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		PresentaseGaji salaryEntity = modelMapper.map(body, PresentaseGaji.class);
		//proses saving data ke database
	
		
		salaryRepository.save(salaryEntity);
		
		body.setIdPresentaseGaji(salaryEntity.getIdPresentaseGaji());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new salary employee  --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/salary/update/{id}")
	public HashMap<String, Object> updateSalary(@PathVariable(value = "id") Integer idPresentaseGaji, @Valid @RequestBody PersentaseGajiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		PresentaseGaji salaryEntity = salaryRepository.findById(idPresentaseGaji).orElseThrow(()-> new ResourceNotFoundException("Salary", "id", idPresentaseGaji));
		
		
		salaryEntity = modelMapper.map(body, PresentaseGaji.class);
		
		salaryEntity.setIdPresentaseGaji(idPresentaseGaji);
	
		
		salaryRepository.save(salaryEntity);
		
		body = modelMapper.map(salaryEntity, PersentaseGajiDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update salary employee --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/salary/delete/{id}")
	public HashMap<String, Object>deleteSalary(@PathVariable(value = "id")Integer idPresentaseGaji,  PersentaseGajiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		PresentaseGaji salaryEntity = salaryRepository.findById(idPresentaseGaji)
				.orElseThrow(()-> new ResourceNotFoundException("Salary", "id", idPresentaseGaji));
		
		salaryEntity = modelMapper.map(body, PresentaseGaji.class);
		salaryEntity.setIdPresentaseGaji(idPresentaseGaji);
		
		salaryRepository.delete(salaryEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete salary employee --> success");
		result.put("Data", "[]");
		
		return result;
		
	}



}
