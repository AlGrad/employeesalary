package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.TingkatanDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.models.Tingkatan;
import com.example.EmployeeSalary.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api")
public class TingkatanController {
	

	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	@GetMapping("/level/readAll")
	public HashMap<String,Object>getAllLevel(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		ArrayList<Tingkatan>listTingkatanEntity = (ArrayList<Tingkatan>) tingkatanRepository.findAll();
				
		
		//Membuat sebuah array list DTO
		ArrayList<TingkatanDTO>listTingkatanDTO = new ArrayList<TingkatanDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(Tingkatan tingkatan : listTingkatanEntity) {
			//inisialisasi obj Publisher DTO 
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);

			
			listTingkatanDTO.add(tingkatanDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all level --> success");
		result.put("Data", listTingkatanDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/level/creates")
	public HashMap<String, Object> createLevel(@Valid @RequestBody TingkatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		Tingkatan tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		//proses saving data ke database
	
		
		tingkatanRepository.save(tingkatanEntity);
		
		body.setIdTingkatan(tingkatanEntity.getIdTingkatan());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new level --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/level/update/{id}")
	public HashMap<String, Object> updateLevel(@PathVariable(value = "id") Integer idTingkatan, @Valid @RequestBody TingkatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan).orElseThrow(()-> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		
		
		tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		
		tingkatanEntity.setIdTingkatan(idTingkatan);
	
		
		tingkatanRepository.save(tingkatanEntity);
		
		body = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update level --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/level/delete/{id}")
	public HashMap<String, Object>deleteLevel(@PathVariable(value = "id")Integer idTingkatan,  TingkatanDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan)
				.orElseThrow(()-> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		
		tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		tingkatanEntity.setIdTingkatan(idTingkatan);
		
		tingkatanRepository.delete(tingkatanEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete level --> success");
		result.put("Data", "[]");
		
		return result;
		
	}


}
