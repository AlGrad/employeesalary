package com.example.EmployeeSalary.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.DTO.TunjanganPegawaiDTO;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.TunjanganPegawai;
import com.example.EmployeeSalary.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@GetMapping("/benefits/readAll")
	public HashMap<String,Object>getAllBenefit(){
		HashMap<String, Object>result = new HashMap<String, Object>();
		
		ArrayList<TunjanganPegawai>listBenefitEntity = (ArrayList<TunjanganPegawai>) tunjanganPegawaiRepository.findAll();
				
		
		//Membuat sebuah array list DTO
		ArrayList<TunjanganPegawaiDTO>listBenefitDTO = new ArrayList<TunjanganPegawaiDTO>();
		
		//mapping semua object dari entity ke DTO menggunakan looping 
		
		for(TunjanganPegawai benefit : listBenefitEntity) {
			//inisialisasi obj Publisher DTO 
			TunjanganPegawaiDTO benefitDTO = modelMapper.map(benefit, TunjanganPegawaiDTO.class);

			
			listBenefitDTO.add(benefitDTO);
			
		}
		
		
		result.put("Status", 200);
		result.put("Message", "Read all employee benefits --> success");
		result.put("Data", listBenefitDTO);
		
		return result;
	}
	
	
	

	
	// CREATE MAPPER
	
	
	
	@PostMapping("/benefit/creates")
	public HashMap<String, Object> createBenefit(@Valid @RequestBody TunjanganPegawaiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
	
		
		TunjanganPegawai benefitEntity = modelMapper.map(body, TunjanganPegawai.class);
		//proses saving data ke database
	
		
		tunjanganPegawaiRepository.save(benefitEntity);
		
		body.setIdTunjanganPegawai(benefitEntity.getIdTunjanganPegawai());
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create new employee benefit --> success");
		result.put("Data", body);
		
		
		return result;
	}

	
	//UPDATE MAPPER
	@PutMapping("/benefit/update/{id}")
	public HashMap<String, Object> updateBenefit(@PathVariable(value = "id") Integer idTunjanganPegawai, @Valid @RequestBody TunjanganPegawaiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		TunjanganPegawai benefitEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai).orElseThrow(()-> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		
		
		benefitEntity = modelMapper.map(body, TunjanganPegawai.class);
		
		benefitEntity.setIdTunjanganPegawai(idTunjanganPegawai);
	
		
		tunjanganPegawaiRepository.save(benefitEntity);
		
		body = modelMapper.map(benefitEntity, TunjanganPegawaiDTO.class);
		
		
		//Mapping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update employee benefit --> success");
		result.put("Data", body);
		
		
		return result;
	}
	
	
	
	@DeleteMapping("/benefit/delete/{id}")
	public HashMap<String, Object>deleteBenefit(@PathVariable(value = "id")Integer idTunjanganPegawai,  TunjanganPegawaiDTO body){
		HashMap<String , Object> result = new HashMap<String, Object>();
		
		
		TunjanganPegawai benefitEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai)
				.orElseThrow(()-> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		
		benefitEntity = modelMapper.map(body, TunjanganPegawai.class);
		benefitEntity.setIdTunjanganPegawai(idTunjanganPegawai);
		
		tunjanganPegawaiRepository.delete(benefitEntity);
		
		
		result.put("Status", 200);
		result.put("Message", "Delete employee benefit --> success");
		result.put("Data", "[]");
		
		return result;
		
	}



}
