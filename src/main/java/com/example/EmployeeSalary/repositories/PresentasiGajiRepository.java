package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.PresentaseGaji;

@Repository
public interface PresentasiGajiRepository extends JpaRepository<PresentaseGaji, Integer> {

}
